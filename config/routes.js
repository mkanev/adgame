/**
 * Created with JetBrains WebStorm.
 * User: Maksim.Kanev
 * Date: 6/3/13
 * Time: 6:31 PM
 * To change this template use File | Settings | File Templates.
 */
var async = require('async')

module.exports = function (app) {

    // articles
    var articles = require("../app/controllers/articles")
    app.get('/api/articles', articles.articles);

    // home
    app.get('/', articles.index);

    var angular = require("../app/controllers/angular")
    app.get('/partials/:name', angular.partials);
    app.get('/templates/:name', angular.templates);
}