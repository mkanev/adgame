/**
 * Created with JetBrains WebStorm.
 * User: Maksim.Kanev
 * Date: 6/3/13
 * Time: 2:11 PM
 * To change this template use File | Settings | File Templates.
 */
angular.module('adgame', ['adgame.filters', 'adgame.services', 'adgame.directives']).
    config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $routeProvider.when('/', {templateUrl: 'partials/partial0', controller: ArticlesCtrl});
        $routeProvider.when('/view1', {templateUrl: 'partials/partial1', controller: MyCtrl1});
        $routeProvider.when('/view2', {templateUrl: 'partials/partial2', controller: MyCtrl2});
        $routeProvider.otherwise({redirectTo: '/'});
        $locationProvider.html5Mode(true);
    }]);

var $isotopeContainer = null;

function fireIsotopeReLayout(container) {
    if (!$isotopeContainer)
        $isotopeContainer = container;
    $isotopeContainer.imagesLoaded(function () {
        $('.element .thumbnail').hover(
            function () {
                $(this).children('.caption').fadeIn(200);
            },
            function () {
                $(this).children('.caption').fadeOut(200);
            }
        );
        $isotopeContainer.isotope({
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
    });
}

$(function () {
    $(window).smartresize(function () {
        if ($isotopeContainer) {
            $isotopeContainer.isotope('reLayout');
        }
    });
});

