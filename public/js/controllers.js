/**
 * Created with JetBrains WebStorm.
 * User: Maksim.Kanev
 * Date: 6/3/13
 * Time: 2:12 PM
 * To change this template use File | Settings | File Templates.
 */
'use strict';

/* Controllers */

function ArticlesCtrl($scope, $http) {
    $http({method: 'GET', url: '/api/articles'}).
        success(function (data, status, headers, config) {
            $scope.items = data.items;
        }).
        error(function (data, status, headers, config) {
            $scope.articles = [];
            $scope.message = 'Error!';
        });
}
function MyCtrl1() {
}
MyCtrl1.$inject = [];


function MyCtrl2() {
}
MyCtrl2.$inject = [];